/*---------------------------------------------------
    
    MUSIC PLAYER #03 by glenthemes
    glen-docs.gitlab.io/player/03
    
    Initial release: 2019/01/29
    Revamp #1 date: 2021/08/27
    Revamp #2 date: 2024/01/20
    Last updated: 2024/01/21
    
    hello gorgeous!
    it's been a tradition of mine to put hidden
    learning resources here in the comments, so
    let's continue that :)
    
    to build a music player you need javascript
    as well as html components. css is wherever you
    want the player to be and how you want it to look!
    
    > audio javascript:
      https://www.w3schools.com/jsref/dom_obj_audio.asp
    > audio html:
      https://www.w3schools.com/tags/tag_audio.asp
    
---------------------------------------------------*/

document.addEventListener("DOMContentLoaded", () => {
  
  document.querySelectorAll("[glenplayer03]")?.forEach(el => {
    
    // get the vars    
    let alwaysShow = getComputedStyle(el).getPropertyValue("--MusicPlayer-Always-Show").replaceAll('"','').replaceAll("'","").trim().toLowerCase();
    
    let state = getComputedStyle(el).getPropertyValue("--MusicPlayer-Start-State").replaceAll('"','').replaceAll("'","").trim().toLowerCase();
    
    let interaction = getComputedStyle(el).getPropertyValue("--MusicPlayer-Interaction-Trigger").replaceAll('"','').replaceAll("'","").trim().toLowerCase();
    
    let apFirst = getComputedStyle(el).getPropertyValue("--Autoplay-First-Song").replaceAll('"','').replaceAll("'","").trim().toLowerCase();
    
    let apNext = getComputedStyle(el).getPropertyValue("--Autoplay-Next-Song").replaceAll('"','').replaceAll("'","").trim().toLowerCase();
    
    let loopList = getComputedStyle(el).getPropertyValue("--Loop-Playlist").replaceAll('"','').replaceAll("'","").trim().toLowerCase();
    
    let titleBar = el.querySelector("[player-title]");
    let list = el.querySelector("[music-list]");
    
    // wrap the list
    if(list){
      let wa = document.createElement("div");
      wa.setAttribute("list-wrapper-a","");
      list.before(wa);
      
      let wb = document.createElement("div");
      wb.setAttribute("list-wrapper-b","");
      wa.append(wb);
      
      wb.append(list);
      
      Array.from(list.childNodes)?.forEach(node => {
        if(node.nodeType === 3 && node.data.trim().length){
          let span = document.createElement("span");
          node.before(span);
          span.appendChild(node);
        }
      })
      
      // interaction stuff
      setTimeout(() => {
        list.classList.add("ready");
        
        if(alwaysShow !== "yes"){
          if(state == "show"){
            el.classList.add("show-on-start");
          } else {
            el.classList.add("hide-on-start")
          }        

          // INTERACTION VIA CLICKS
          if(interaction == "click"){
            el.setAttribute("click-activ","");
            if(titleBar){
              state == "show" ? titleBar.classList.add("clicked") : "";            
              titleBar.addEventListener("click", () => {
                if(!titleBar.matches(".clicked")){
                  titleBar.classList.add("clicked")
                } else {
                  titleBar.classList.remove("clicked")
                }
              })
            }
          }//end: if click activ

          else {
            // INTERACTION VIA HOVER
            el.setAttribute("hov-activ","");

            function mouseout(){
              el.classList.remove("show-on-start");
              el.removeEventListener("mouseenter",mouseout);
            }

            el.addEventListener("mouseenter",mouseout)
          }
        }
        
      },0);      
    }//end: if [music-list] exists
    
    let iconStyle = getComputedStyle(el).getPropertyValue("--MusicPlayer-Icon-Style").replaceAll('"','').replaceAll("'","").trim().toLowerCase();
    iconStyle = iconStyle === "" || iconStyle === "filled" ? "fill" :
                iconStyle === "outline" || iconStyle === "outlined" ? "outline" :
                "fill"; // default to 'fill' if input is invalid
    
    el.querySelectorAll("[song-row]")?.forEach(row => {
      let c = document.createElement("div");
      c.setAttribute("music-controls","");
      row.append(c);
      
      let playBtn = document.createElement("button");
      playBtn.setAttribute("play-btn","");
      c.append(playBtn);
      
      let p1 = document.createElement("i");
      p1.classList.add(`g-play-${iconStyle}`);
      playBtn.append(p1);
      
      let pauseBtn = document.createElement("button");
      pauseBtn.setAttribute("pause-btn","");
      c.append(pauseBtn);
      
      let p2 = document.createElement("i");
      p2.classList.add(`g-pause-${iconStyle}`);
      pauseBtn.append(p2);
      
      let audio = row.querySelector("audio[src]:not([src=''])");
      if(audio){
        let audSrc = audio.src;
        
        // fix dropbox links
        if(audSrc.indexOf("//www.dropbox.com") > -1){
          audSrc = audSrc.replace("//www.dropbox.com","//dl.dropbox.com").replace("&dl=0","");
          audio.src = audSrc;
        }
        
        // check if URL is valid
        // volume
        if(audio.matches("[volume]:not([volume=''])")){
          let vol = audio.getAttribute("volume").replace(/[^\d\.]*/g,"");
          vol = Number(vol);
          if(!isNaN(vol)){
            // if there's a "%" in the string
            if(audio.getAttribute("volume").trim().indexOf("%") > -1){
              vol = vol/100;
            }

            audio.volume = vol;
          }
        }

        // if: autoplay first song
        if(!row.previousElementSibling || !row.previousElementSibling.matches("[song-row]")){
          if(apFirst == "yes" || apFirst == "true"){
            let joue = () => {
              audio.play();
              document.documentElement.removeEventListener("mouseover",joue);
              document.documentElement.removeEventListener("click",joue);
              document.documentElement.removeEventListener("focus",joue);
              document.documentElement.removeEventListener("keydown",joue);
            }

            document.documentElement.addEventListener("mouseover",joue);
            document.documentElement.addEventListener("click",joue);
            document.documentElement.addEventListener("focus",joue);                
            document.documentElement.addEventListener("keydown",joue);
          }
        }
        
        audio.addEventListener("loadedmetadata", () => {
          playBtn.addEventListener("click", () => {
            if(audio.paused){
              audio.play();
            }
          })

          pauseBtn.addEventListener("click", () => {
            if(!audio.paused){
              audio.pause();
            }
          })

          audio.addEventListener("play", () => {
            playBtn.style.display = "none";
            pauseBtn.style.display = "block";
          })

          audio.addEventListener("pause", () => {
            playBtn.style.display = "block";
            pauseBtn.style.display = "none";
          })

          if(!audio.matches("[loop]")){
            audio.addEventListener("ended", () => {
              playBtn.style.display = "block";
              pauseBtn.style.display = "none";

              // autoplay next song y/n
              if(apNext == "yes" || apNext == "true"){
                let next = row.nextElementSibling;
                if(next && next.matches("[song-row]")){
                  let nextAud = next.querySelector("audio[src]:not([src=''])");
                  nextAud ? nextAud.play() : ""
                }

                else {
                  // loop playlist y/n
                  if(loopList == "yes" || loopList == "true"){
                    list.querySelector("[song-row] audio[src]:not([src=''])")?.play();
                  }
                }//end if loop playlist
              }//end if apNext
            })//end onended
          }//end: if current song doesn't have [loop] enabled
        })//end: audio loaded

          
      }//end if <audio> exists
    })//end row forEach
  })//end [glenplayer03] forEach
  
  playerTitleHeight(2000);
})//end DOMContentLoaded

const playerTitleHeight = (t) => {
  let tbar = document.querySelector("[glenplayer03] [player-title]");
  if(tbar){
    let arfsx = Date.now();
    let goczd = Number(t);
    let uvaay = setInterval(() => {
      if(Date.now - arfsx > goczd){
        clearInterval(uvaay)
      } else {
        let h = tbar.offsetHeight;
        if(h > 1){
          clearInterval(uvaay);
          tbar.closest("[glenplayer03]").style.setProperty("--MusicPlayer-Title-Height",`${h}px`);
        }
      }
    },0)
  }
}

window.addEventListener("resize", () => {
  playerTitleHeight(1);
})

document.addEventListener("play", (e) => {
  let audios = document.getElementsByTagName("audio");
  for(let a of audios){
    if(a !== e.target){
      a.pause();
      a.currentTime = 0;
    }
  }
},true);
